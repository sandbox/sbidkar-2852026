/**
 * @file
 * Implements Clock Functions.
 */

  function ClockOne() {
    'use strict';
    var offset = document.getElementById('timezone-offset-1').innerHTML;
    var format = document.getElementById('formated-1').innerHTML;
    var now = new Date();
    var hour = 60 * 60 * 1000;
    var min = 60 * 1000;
    var today = new Date(now.getTime() + (now.getTimezoneOffset() * min) + (offset * hour));
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTimeOne(m);
    s = checkTimeOne(s);
    h = ('0' + h).slice(-2);
    m = ('0' + m).slice(-2);
    s = ('0' + s).slice(-2);
    var clock = h + ':' + m + ':' + s;
    if (format === 12) {
      clock = time_format_one(clock);
    }
    document.getElementById('clock-1').innerHTML = clock;
    setTimeout(ClockOne, 500);
  }

  function checkTimeOne(i) {
    'use strict';
    if (i < 10) {
      i = '0' + i;
    }  // add zero in front of numbers < 10
    return i;
  }

  function time_format_one(timeString) {
    'use strict';
    var H = +timeString.substr(0, 2);
    var h = H % 12 || 12;
    var ampm = H < 12 ? ' AM' : ' PM';
    timeString = h + timeString.substr(2, 3) + ampm;
    return timeString;
  }



  function ClockTwo() {
    'use strict';
    var offset = document.getElementById('timezone-offset-2').innerHTML;
    var format = document.getElementById('formated-2').innerHTML;
    var now = new Date();
    var hour = 60 * 60 * 1000;
    var min = 60 * 1000;
    var today = new Date(now.getTime() + (now.getTimezoneOffset() * min) + (offset * hour));
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTimeTwo(m);
    s = checkTimeTwo(s);
    h = ('0' + h).slice(-2);
    m = ('0' + m).slice(-2);
    s = ('0' + s).slice(-2);
    var clock = h + ':' + m + ':' + s;
    if (format === 12) {
      clock = time_format_two(clock);
    }
    document.getElementById('clock-2').innerHTML = clock;
    setTimeout(ClockTwo, 500);
  }

  function checkTimeTwo(i) {
    'use strict';
    if (i < 10) {
      i = '0' + i;
    }  // add zero in front of numbers < 10
    return i;
  }

  function time_format_two(timeString) {
    'use strict';
    var H = +timeString.substr(0, 2);
    var h = H % 12 || 12;
    var ampm = H < 12 ? ' AM' : ' PM';
    timeString = h + timeString.substr(2, 3) + ampm;
    return timeString;
  }



  function ClockThree() {
    'use strict';
    var offset = document.getElementById('timezone-offset-3').innerHTML;
    var format = document.getElementById('formated-3').innerHTML;
    var now = new Date();
    var hour = 60 * 60 * 1000;
    var min = 60 * 1000;
    var today = new Date(now.getTime() + (now.getTimezoneOffset() * min) + (offset * hour));
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTimeThree(m);
    s = checkTimeThree(s);
    h = ('0' + h).slice(-2);
    m = ('0' + m).slice(-2);
    s = ('0' + s).slice(-2);
    var clock = h + ':' + m + ':' + s;
    if (format === 12) {
      clock = time_format_three(clock);
    }
    document.getElementById('clock-3').innerHTML = clock;
    setTimeout(ClockThree, 500);
  }

  function checkTimeThree(i) {
    'use strict';
    if (i < 10) {
      i = '0' + i;
    }  // add zero in front of numbers < 10
    return i;
  }

  function time_format_three(timeString) {
    'use strict';
    var H = +timeString.substr(0, 2);
    var h = H % 12 || 12;
    var ampm = H < 12 ? ' AM' : ' PM';
    timeString = h + timeString.substr(2, 3) + ampm;
    return timeString;
  }
