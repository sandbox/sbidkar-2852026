<?php

namespace Drupal\timezone_clock\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides the  Block 'ClockBlock'.
 *
 * @Block(
 *   id = "TimezoneClockBlock",
 *   subject = @Translation("Timezone Clock Block"),
 *   admin_label = @Translation("Timezone Clock")
 * )
 */
class TimezoneClockBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $offset = '+05.30';
    $offset = '';
    $timeformat = '';
    $markup = '';
    $n = 1;
    $query = "select country, offset, timezone, time_format from timezone_clock_blocks";
    $result = db_query($query);
    foreach ($result as $rec) {
      $offset = $rec->offset;
      $timeformat = $rec->time_format;
      $country = $rec->country;

      $markup .= '<div id="zone">' . $country . '</div>
      <div id="formated-' . $n . '">' . $timeformat . '</div>
      <div id="timezone-offset-' . $n . '">' . $offset . '</div>
      <div class="timezone-clock" id="clock-' . $n . '"></div>
      <div> <hr> </div>';
      $n++;
    }

    $output = array(
      '#type' => 'markup',
      '#markup' => $markup,
      '#cache' => array('max-age' => 0),
      '#attached' => array(
        'library' => array(
          'timezone_clock/timezone_clock.scripts',
          'timezone_clock/timezone_clock.styles',
        ),
      ),
    );

    return $output;
  }

}
