<?php

namespace Drupal\timezone_clock\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Contribute form.
 */
class TimezoneClockForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'my_timezone_clock_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#attached']['library'][] = 'advance_world_clock/advance_world_clock.scripts';
    $form['#attached']['library'][] = 'advance_world_clock/advance_world_clock.styles';
    $contry_list = array('' => '- Select Country -');
    $zones_select = array('' => '- Select Zone -');
    $zones = system_time_zones();
    $zones = array_merge($zones_select, $zones);
    $countries = \Drupal\Core\Locale\CountryManager::getStandardList();
    foreach ($countries as $value) {
      $country_name = (string) $value;
      $contry_list[$country_name] = $country_name;
    }

    $i = 0;
    $name_field = $form_state->get('num_names');
    $form['#tree'] = TRUE;
    $form['names_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Set multiple clocks for timezone'),
      '#prefix' => '<div id="names-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];
    $form['names_fieldset']['form_inner_fieldset'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="inner-fieldset-wrapper">',
      '#suffix' => '</div>',
    );

    $name_field = 3;
    for ($i = 0; $i < $name_field; $i++) {
      $form['names_fieldset']['form_inner_fieldset'][$i]['timezone'] = array(
        '#type' => 'select',
        '#title' => $this->t('Select time zone'),
        // '#default_value' => $default_zone,.
        '#options' => $zones,
        '#prefix' => '<div class="field-wrapper">',
        '#suffix' => '</div>',
      );

      $form['names_fieldset']['form_inner_fieldset'][$i]['country_name'] = array(
        '#type' => 'select',
        '#title' => $this->t('Country Name'),
        '#attributes' => array(),
        '#options' => $contry_list,
        '#prefix' => '<div class="field-wrapper">',
        '#suffix' => '</div>',
      );

      $form['names_fieldset']['form_inner_fieldset'][$i]['timeformat'] = array(
        '#type' => 'radios',
        '#title' => $this->t('Time format'),
        '#default_value' => '24',
        '#options' => array('24' => $this->t('24 hr'), '12' => $this->t('12 hr')),
        '#attributes' => array(),
        '#prefix' => '<div class="field-wrapper">',
        '#suffix' => '</div><hr>',
      );
    } // end for
    $form_state->setCached(FALSE);
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $zone_array['Africa/Abidjan'] = '+00:00';
    $zone_array['Africa/Accra'] = '+00:00';
    $zone_array['Africa/Addis_Ababa'] = '+03:00';
    $zone_array['Africa/Algiers'] = '+01:00';
    $zone_array['Africa/Asmara'] = '+03:00';
    $zone_array['Africa/Asmera'] = '+03:00';
    $zone_array['Africa/Bamako'] = '+00:00';
    $zone_array['Africa/Bangui'] = '+01:00';
    $zone_array['Africa/Banjul'] = '+00:00';
    $zone_array['Africa/Bissau'] = '+00:00';
    $zone_array['Africa/Blantyre'] = '+02:00';
    $zone_array['Africa/Brazzaville'] = '+01:00';
    $zone_array['Africa/Bujumbura'] = '+02:00';
    $zone_array['Africa/Cairo'] = '+02:00';
    $zone_array['Africa/Casablanca'] = '+00:00';
    $zone_array['Africa/Ceuta'] = '+01:00';
    $zone_array['Africa/Conakry'] = '+00:00';
    $zone_array['Africa/Dakar'] = '+00:00';
    $zone_array['Africa/Dar_es_Salaam'] = '+03:00';
    $zone_array['Africa/Djibouti'] = '+03:00';
    $zone_array['Africa/Douala'] = '+01:00';
    $zone_array['Africa/El_Aaiun'] = '+00:00';
    $zone_array['Africa/Freetown'] = '+00:00';
    $zone_array['Africa/Gaborone'] = '+02:00';
    $zone_array['Africa/Harare'] = '+02:00';
    $zone_array['Africa/Johannesburg'] = '+02:00';
    $zone_array['Africa/Juba'] = '+03:00';
    $zone_array['Africa/Kampala'] = '+03:00';
    $zone_array['Africa/Khartoum'] = '+03:00';
    $zone_array['Africa/Kigali'] = '+02:00';
    $zone_array['Africa/Kinshasa'] = '+01:00';
    $zone_array['Africa/Lagos'] = '+01:00';
    $zone_array['Africa/Libreville'] = '+01:00';
    $zone_array['Africa/Lome'] = '+00:00';
    $zone_array['Africa/Luanda'] = '+01:00';
    $zone_array['Africa/Lubumbashi'] = '+02:00';
    $zone_array['Africa/Lusaka'] = '+02:00';
    $zone_array['Africa/Malabo'] = '+01:00';
    $zone_array['Africa/Maputo'] = '+02:00';
    $zone_array['Africa/Maseru'] = '+02:00';
    $zone_array['Africa/Mbabane'] = '+02:00';
    $zone_array['Africa/Mogadishu'] = '+03:00';
    $zone_array['Africa/Monrovia'] = '+00:00';
    $zone_array['Africa/Nairobi'] = '+03:00';
    $zone_array['Africa/Ndjamena'] = '+01:00';
    $zone_array['Africa/Niamey'] = '+01:00';
    $zone_array['Africa/Nouakchott'] = '+00:00';
    $zone_array['Africa/Ouagadougou'] = '+00:00';
    $zone_array['Africa/Porto-Novo'] = '+01:00';
    $zone_array['Africa/Sao_Tome'] = '+00:00';
    $zone_array['Africa/Timbuktu'] = '+00:00';
    $zone_array['Africa/Tripoli'] = '+02:00';
    $zone_array['Africa/Tunis'] = '+01:00';
    $zone_array['Africa/Windhoek'] = '+01:00';
    $zone_array['AKST9AKDT'] = '-09:00';
    $zone_array['America/Adak'] = '-10:00';
    $zone_array['America/Anchorage'] = '-09:00';
    $zone_array['America/Anguilla'] = '-04:00';
    $zone_array['America/Antigua'] = '-04:00';
    $zone_array['America/Araguaina'] = '-03:00';
    $zone_array['America/Argentina/Buenos_Aires'] = '-03:00';
    $zone_array['America/Argentina/Catamarca'] = '-03:00';
    $zone_array['America/Argentina/ComodRivadavia'] = '-03:00';
    $zone_array['America/Argentina/Cordoba'] = '-03:00';
    $zone_array['America/Argentina/Jujuy'] = '-03:00';
    $zone_array['America/Argentina/La_Rioja'] = '-03:00';
    $zone_array['America/Argentina/Mendoza'] = '-03:00';
    $zone_array['America/Argentina/Rio_Gallegos'] = '-03:00';
    $zone_array['America/Argentina/Salta'] = '-03:00';
    $zone_array['America/Argentina/San_Juan'] = '-03:00';
    $zone_array['America/Argentina/San_Luis'] = '-03:00';
    $zone_array['America/Argentina/Tucuman'] = '-03:00';
    $zone_array['America/Argentina/Ushuaia'] = '-03:00';
    $zone_array['America/Aruba'] = '-04:00';
    $zone_array['America/Asuncion'] = '-04:00';
    $zone_array['America/Atikokan'] = '-05:00';
    $zone_array['America/Atka'] = '-10:00';
    $zone_array['America/Bahia'] = '-03:00';
    $zone_array['America/Bahia_Banderas'] = '-06:00';
    $zone_array['America/Barbados'] = '-04:00';
    $zone_array['America/Belem'] = '-03:00';
    $zone_array['America/Belize'] = '-06:00';
    $zone_array['America/Blanc-Sablon'] = '-04:00';
    $zone_array['America/Boa_Vista'] = '-04:00';
    $zone_array['America/Bogota'] = '-05:00';
    $zone_array['America/Boise'] = '-07:00';
    $zone_array['America/Buenos_Aires'] = '-03:00';
    $zone_array['America/Cambridge_Bay'] = '-07:00';
    $zone_array['America/Campo_Grande'] = '-04:00';
    $zone_array['America/Cancun'] = '-06:00';
    $zone_array['America/Caracas'] = '-04:30';
    $zone_array['America/Catamarca'] = '-03:00';
    $zone_array['America/Cayenne'] = '-03:00';
    $zone_array['America/Cayman'] = '-05:00';
    $zone_array['America/Chicago'] = '-06:00';
    $zone_array['America/Chihuahua'] = '-07:00';
    $zone_array['America/Coral_Harbour'] = '-05:00';
    $zone_array['America/Cordoba'] = '-03:00';
    $zone_array['America/Costa_Rica'] = '-06:00';
    $zone_array['America/Creston'] = '-07:00';
    $zone_array['America/Cuiaba'] = '-04:00';
    $zone_array['America/Curacao'] = '-04:00';
    $zone_array['America/Danmarkshavn'] = '+00:00';
    $zone_array['America/Dawson'] = '-08:00';
    $zone_array['America/Dawson_Creek'] = '-07:00';
    $zone_array['America/Denver'] = '-07:00';
    $zone_array['America/Detroit'] = '-05:00';
    $zone_array['America/Dominica'] = '-04:00';
    $zone_array['America/Edmonton'] = '-07:00';
    $zone_array['America/Eirunepe'] = '-04:00';
    $zone_array['America/El_Salvador'] = '-06:00';
    $zone_array['America/Ensenada'] = '-08:00';
    $zone_array['America/Fort_Wayne'] = '-05:00';
    $zone_array['America/Fortaleza'] = '-03:00';
    $zone_array['America/Glace_Bay'] = '-04:00';
    $zone_array['America/Godthab'] = '-03:00';
    $zone_array['America/Goose_Bay'] = '-04:00';
    $zone_array['America/Grand_Turk'] = '-05:00';
    $zone_array['America/Grenada'] = '-04:00';
    $zone_array['America/Guadeloupe'] = '-04:00';
    $zone_array['America/Guatemala'] = '-06:00';
    $zone_array['America/Guayaquil'] = '-05:00';
    $zone_array['America/Guyana'] = '-04:00';
    $zone_array['America/Halifax'] = '-04:00';
    $zone_array['America/Havana'] = '-05:00';
    $zone_array['America/Hermosillo'] = '-07:00';
    $zone_array['America/Indiana/Indianapolis'] = '-05:00';
    $zone_array['America/Indiana/Knox'] = '-06:00';
    $zone_array['America/Indiana/Marengo'] = '-05:00';
    $zone_array['America/Indiana/Petersburg'] = '-05:00';
    $zone_array['America/Indiana/Tell_City'] = '-06:00';
    $zone_array['America/Indiana/Vevay'] = '-05:00';
    $zone_array['America/Indiana/Vincennes'] = '-05:00';
    $zone_array['America/Indiana/Winamac'] = '-05:00';
    $zone_array['America/Indianapolis'] = '-05:00';
    $zone_array['America/Inuvik'] = '-07:00';
    $zone_array['America/Iqaluit'] = '-05:00';
    $zone_array['America/Jamaica'] = '-05:00';
    $zone_array['America/Jujuy'] = '-03:00';
    $zone_array['America/Juneau'] = '-09:00';
    $zone_array['America/Kentucky/Louisville'] = '-05:00';
    $zone_array['America/Kentucky/Monticello'] = '-05:00';
    $zone_array['America/Knox_IN'] = '-06:00';
    $zone_array['America/Kralendijk'] = '-04:00';
    $zone_array['America/La_Paz'] = '-04:00';
    $zone_array['America/Lima'] = '-05:00';
    $zone_array['America/Los_Angeles'] = '-08:00';
    $zone_array['America/Louisville'] = '-05:00';
    $zone_array['America/Lower_Princes'] = '-04:00';
    $zone_array['America/Maceio'] = '-03:00';
    $zone_array['America/Managua'] = '-06:00';
    $zone_array['America/Manaus'] = '-04:00';
    $zone_array['America/Marigot'] = '-04:00';
    $zone_array['America/Martinique'] = '-04:00';
    $zone_array['America/Matamoros'] = '-06:00';
    $zone_array['America/Mazatlan'] = '-07:00';
    $zone_array['America/Mendoza'] = '-03:00';
    $zone_array['America/Menominee'] = '-06:00';
    $zone_array['America/Merida'] = '-06:00';
    $zone_array['America/Metlakatla'] = '-08:00';
    $zone_array['America/Mexico_City'] = '-06:00';
    $zone_array['America/Miquelon'] = '-03:00';
    $zone_array['America/Moncton'] = '-04:00';
    $zone_array['America/Monterrey'] = '-06:00';
    $zone_array['America/Montevideo'] = '-03:00';
    $zone_array['America/Montreal'] = '-05:00';
    $zone_array['America/Montserrat'] = '-04:00';
    $zone_array['America/Nassau'] = '-05:00';
    $zone_array['America/New_York'] = '-05:00';
    $zone_array['America/Nipigon'] = '-05:00';
    $zone_array['America/Nome'] = '-09:00';
    $zone_array['America/Noronha'] = '-02:00';
    $zone_array['America/North_Dakota/Beulah'] = '-06:00';
    $zone_array['America/North_Dakota/Center'] = '-06:00';
    $zone_array['America/North_Dakota/New_Salem'] = '-06:00';
    $zone_array['America/Ojinaga'] = '-07:00';
    $zone_array['America/Panama'] = '-05:00';
    $zone_array['America/Pangnirtung'] = '-05:00';
    $zone_array['America/Paramaribo'] = '-03:00';
    $zone_array['America/Phoenix'] = '-07:00';
    $zone_array['America/Port_of_Spain'] = '-04:00';
    $zone_array['America/Port-au-Prince'] = '-05:00';
    $zone_array['America/Porto_Acre'] = '-04:00';
    $zone_array['America/Porto_Velho'] = '-04:00';
    $zone_array['America/Puerto_Rico'] = '-04:00';
    $zone_array['America/Rainy_River'] = '-06:00';
    $zone_array['America/Rankin_Inlet'] = '-06:00';
    $zone_array['America/Recife'] = '-03:00';
    $zone_array['America/Regina'] = '-06:00';
    $zone_array['America/Resolute'] = '-06:00';
    $zone_array['America/Rio_Branco'] = '-04:00';
    $zone_array['America/Rosario'] = '-03:00';
    $zone_array['America/Santa_Isabel'] = '-08:00';
    $zone_array['America/Santarem'] = '-03:00';
    $zone_array['America/Santiago'] = '-04:00';
    $zone_array['America/Santo_Domingo'] = '-04:00';
    $zone_array['America/Sao_Paulo'] = '-03:00';
    $zone_array['America/Scoresbysund'] = '-01:00';
    $zone_array['America/Shiprock'] = '-07:00';
    $zone_array['America/Sitka'] = '-09:00';
    $zone_array['America/St_Barthelemy'] = '-04:00';
    $zone_array['America/St_Johns'] = '-03:30';
    $zone_array['America/St_Kitts'] = '-04:00';
    $zone_array['America/St_Lucia'] = '-04:00';
    $zone_array['America/St_Thomas'] = '-04:00';
    $zone_array['America/St_Vincent'] = '-04:00';
    $zone_array['America/Swift_Current'] = '-06:00';
    $zone_array['America/Tegucigalpa'] = '-06:00';
    $zone_array['America/Thule'] = '-04:00';
    $zone_array['America/Thunder_Bay'] = '-05:00';
    $zone_array['America/Tijuana'] = '-08:00';
    $zone_array['America/Toronto'] = '-05:00';
    $zone_array['America/Tortola'] = '-04:00';
    $zone_array['America/Vancouver'] = '-08:00';
    $zone_array['America/Virgin'] = '-04:00';
    $zone_array['America/Whitehorse'] = '-08:00';
    $zone_array['America/Winnipeg'] = '-06:00';
    $zone_array['America/Yakutat'] = '-09:00';
    $zone_array['America/Yellowknife'] = '-07:00';
    $zone_array['Antarctica/Casey'] = '+11:00';
    $zone_array['Antarctica/Davis'] = '+05:00';
    $zone_array['Antarctica/DumontDUrville'] = '+10:00';
    $zone_array['Antarctica/Macquarie'] = '+11:00';
    $zone_array['Antarctica/Mawson'] = '+05:00';
    $zone_array['Antarctica/McMurdo'] = '+12:00';
    $zone_array['Antarctica/Palmer'] = '-04:00';
    $zone_array['Antarctica/Rothera'] = '-03:00';
    $zone_array['Antarctica/South_Pole'] = '+12:00';
    $zone_array['Antarctica/Syowa'] = '+03:00';
    $zone_array['Antarctica/Vostok'] = '+06:00';
    $zone_array['Arctic/Longyearbyen'] = '+01:00';
    $zone_array['Asia/Aden'] = '+03:00';
    $zone_array['Asia/Almaty'] = '+06:00';
    $zone_array['Asia/Amman'] = '+02:00';
    $zone_array['Asia/Anadyr'] = '+12:00';
    $zone_array['Asia/Aqtau'] = '+05:00';
    $zone_array['Asia/Aqtobe'] = '+05:00';
    $zone_array['Asia/Ashgabat'] = '+05:00';
    $zone_array['Asia/Ashkhabad'] = '+05:00';
    $zone_array['Asia/Baghdad'] = '+03:00';
    $zone_array['Asia/Bahrain'] = '+03:00';
    $zone_array['Asia/Baku'] = '+04:00';
    $zone_array['Asia/Bangkok'] = '+07:00';
    $zone_array['Asia/Beirut'] = '+02:00';
    $zone_array['Asia/Bishkek'] = '+06:00';
    $zone_array['Asia/Brunei'] = '+08:00';
    $zone_array['Asia/Calcutta'] = '+05:30';
    $zone_array['Asia/Choibalsan'] = '+08:00';
    $zone_array['Asia/Chongqing'] = '+08:00';
    $zone_array['Asia/Chungking'] = '+08:00';
    $zone_array['Asia/Colombo'] = '+05:30';
    $zone_array['Asia/Dacca'] = '+06:00';
    $zone_array['Asia/Damascus'] = '+02:00';
    $zone_array['Asia/Dhaka'] = '+06:00';
    $zone_array['Asia/Dili'] = '+09:00';
    $zone_array['Asia/Dubai'] = '+04:00';
    $zone_array['Asia/Dushanbe'] = '+05:00';
    $zone_array['Asia/Gaza'] = '+02:00';
    $zone_array['Asia/Harbin'] = '+08:00';
    $zone_array['Asia/Hebron'] = '+02:00';
    $zone_array['Asia/Ho_Chi_Minh'] = '+07:00';
    $zone_array['Asia/Hong_Kong'] = '+08:00';
    $zone_array['Asia/Hovd'] = '+07:00';
    $zone_array['Asia/Irkutsk'] = '+09:00';
    $zone_array['Asia/Istanbul'] = '+02:00';
    $zone_array['Asia/Jakarta'] = '+07:00';
    $zone_array['Asia/Jayapura'] = '+09:00';
    $zone_array['Asia/Jerusalem'] = '+02:00';
    $zone_array['Asia/Kabul'] = '+04:30';
    $zone_array['Asia/Kamchatka'] = '+12:00';
    $zone_array['Asia/Karachi'] = '+05:00';
    $zone_array['Asia/Kashgar'] = '+08:00';
    $zone_array['Asia/Kathmandu'] = '+05:45';
    $zone_array['Asia/Katmandu'] = '+05:45';
    $zone_array['Asia/Kolkata'] = '+05:30';
    $zone_array['Asia/Krasnoyarsk'] = '+08:00';
    $zone_array['Asia/Kuala_Lumpur'] = '+08:00';
    $zone_array['Asia/Kuching'] = '+08:00';
    $zone_array['Asia/Kuwait'] = '+03:00';
    $zone_array['Asia/Macao'] = '+08:00';
    $zone_array['Asia/Macau'] = '+08:00';
    $zone_array['Asia/Magadan'] = '+12:00';
    $zone_array['Asia/Makassar'] = '+08:00';
    $zone_array['Asia/Manila'] = '+08:00';
    $zone_array['Asia/Muscat'] = '+04:00';
    $zone_array['Asia/Nicosia'] = '+02:00';
    $zone_array['Asia/Novokuznetsk'] = '+07:00';
    $zone_array['Asia/Novosibirsk'] = '+07:00';
    $zone_array['Asia/Omsk'] = '+07:00';
    $zone_array['Asia/Oral'] = '+05:00';
    $zone_array['Asia/Phnom_Penh'] = '+07:00';
    $zone_array['Asia/Pontianak'] = '+07:00';
    $zone_array['Asia/Pyongyang'] = '+09:00';
    $zone_array['Asia/Qatar'] = '+03:00';
    $zone_array['Asia/Qyzylorda'] = '+06:00';
    $zone_array['Asia/Rangoon'] = '+06:30';
    $zone_array['Asia/Riyadh'] = '+03:00';
    $zone_array['Asia/Saigon'] = '+07:00';
    $zone_array['Asia/Sakhalin'] = '+11:00';
    $zone_array['Asia/Samarkand'] = '+05:00';
    $zone_array['Asia/Seoul'] = '+09:00';
    $zone_array['Asia/Shanghai'] = '+08:00';
    $zone_array['Asia/Singapore'] = '+08:00';
    $zone_array['Asia/Taipei'] = '+08:00';
    $zone_array['Asia/Tashkent'] = '+05:00';
    $zone_array['Asia/Tbilisi'] = '+04:00';
    $zone_array['Asia/Tehran'] = '+03:30';
    $zone_array['Asia/Tel_Aviv'] = '+02:00';
    $zone_array['Asia/Thimbu'] = '+06:00';
    $zone_array['Asia/Thimphu'] = '+06:00';
    $zone_array['Asia/Tokyo'] = '+09:00';
    $zone_array['Asia/Ujung_Pandang'] = '+08:00';
    $zone_array['Asia/Ulaanbaatar'] = '+08:00';
    $zone_array['Asia/Ulan_Bator'] = '+08:00';
    $zone_array['Asia/Urumqi'] = '+08:00';
    $zone_array['Asia/Vientiane'] = '+07:00';
    $zone_array['Asia/Vladivostok'] = '+11:00';
    $zone_array['Asia/Yakutsk'] = '+10:00';
    $zone_array['Asia/Yekaterinburg'] = '+06:00';
    $zone_array['Asia/Yerevan'] = '+04:00';
    $zone_array['Atlantic/Azores'] = '-01:00';
    $zone_array['Atlantic/Bermuda'] = '-04:00';
    $zone_array['Atlantic/Canary'] = '+00:00';
    $zone_array['Atlantic/Cape_Verde'] = '-01:00';
    $zone_array['Atlantic/Faeroe'] = '+00:00';
    $zone_array['Atlantic/Faroe'] = '+00:00';
    $zone_array['Atlantic/Jan_Mayen'] = '+01:00';
    $zone_array['Atlantic/Madeira'] = '+00:00';
    $zone_array['Atlantic/Reykjavik'] = '+00:00';
    $zone_array['Atlantic/South_Georgia'] = '-02:00';
    $zone_array['Atlantic/St_Helena'] = '+00:00';
    $zone_array['Atlantic/Stanley'] = '-03:00';
    $zone_array['Australia/ACT'] = '+10:00';
    $zone_array['Australia/Adelaide'] = '+09:30';
    $zone_array['Australia/Brisbane'] = '+10:00';
    $zone_array['Australia/Broken_Hill'] = '+09:30';
    $zone_array['Australia/Canberra'] = '+10:00';
    $zone_array['Australia/Currie'] = '+10:00';
    $zone_array['Australia/Darwin'] = '+09:30';
    $zone_array['Australia/Eucla'] = '+08:45';
    $zone_array['Australia/Hobart'] = '+10:00';
    $zone_array['Australia/LHI'] = '+10:30';
    $zone_array['Australia/Lindeman'] = '+10:00';
    $zone_array['Australia/Lord_Howe'] = '+10:30';
    $zone_array['Australia/Melbourne'] = '+10:00';
    $zone_array['Australia/North'] = '+09:30';
    $zone_array['Australia/NSW'] = '+10:00';
    $zone_array['Australia/Perth'] = '+08:00';
    $zone_array['Australia/Queensland'] = '+10:00';
    $zone_array['Australia/South'] = '+09:30';
    $zone_array['Australia/Sydney'] = '+10:00';
    $zone_array['Australia/Tasmania'] = '+10:00';
    $zone_array['Australia/Victoria'] = '+10:00';
    $zone_array['Australia/West'] = '+08:00';
    $zone_array['Australia/Yancowinna'] = '+09:30';
    $zone_array['Brazil/Acre'] = '-04:00';
    $zone_array['Brazil/DeNoronha'] = '-02:00';
    $zone_array['Brazil/East'] = '-03:00';
    $zone_array['Brazil/West'] = '-04:00';
    $zone_array['Canada/Atlantic'] = '-04:00';
    $zone_array['Canada/Central'] = '-06:00';
    $zone_array['Canada/Eastern'] = '-05:00';
    $zone_array['Canada/East-Saskatchewan'] = '-06:00';
    $zone_array['Canada/Mountain'] = '-07:00';
    $zone_array['Canada/Newfoundland'] = '-03:30';
    $zone_array['Canada/Pacific'] = '-08:00';
    $zone_array['Canada/Saskatchewan'] = '-06:00';
    $zone_array['Canada/Yukon'] = '-08:00';
    $zone_array['CET'] = '+01:00';
    $zone_array['Chile/Continental'] = '-04:00';
    $zone_array['Chile/EasterIsland'] = '-06:00';
    $zone_array['CST6CDT'] = '-06:00';
    $zone_array['Cuba'] = '-05:00';
    $zone_array['EET'] = '+02:00';
    $zone_array['Egypt'] = '+02:00';
    $zone_array['Eire'] = '+00:00';
    $zone_array['EST'] = '-05:00';
    $zone_array['EST5EDT'] = '-05:00';
    $zone_array['Etc./GMT'] = '+00:00';
    $zone_array['Etc./GMT+0'] = '+00:00';
    $zone_array['Etc./UCT'] = '+00:00';
    $zone_array['Etc./Universal'] = '+00:00';
    $zone_array['Etc./UTC'] = '+00:00';
    $zone_array['Etc./Zulu'] = '+00:00';
    $zone_array['Europe/Amsterdam'] = '+01:00';
    $zone_array['Europe/Andorra'] = '+01:00';
    $zone_array['Europe/Athens'] = '+02:00';
    $zone_array['Europe/Belfast'] = '+00:00';
    $zone_array['Europe/Belgrade'] = '+01:00';
    $zone_array['Europe/Berlin'] = '+01:00';
    $zone_array['Europe/Bratislava'] = '+01:00';
    $zone_array['Europe/Brussels'] = '+01:00';
    $zone_array['Europe/Bucharest'] = '+02:00';
    $zone_array['Europe/Budapest'] = '+01:00';
    $zone_array['Europe/Chisinau'] = '+02:00';
    $zone_array['Europe/Copenhagen'] = '+01:00';
    $zone_array['Europe/Dublin'] = '+00:00';
    $zone_array['Europe/Gibraltar'] = '+01:00';
    $zone_array['Europe/Guernsey'] = '+00:00';
    $zone_array['Europe/Helsinki'] = '+02:00';
    $zone_array['Europe/Isle_of_Man'] = '+00:00';
    $zone_array['Europe/Istanbul'] = '+02:00';
    $zone_array['Europe/Jersey'] = '+00:00';
    $zone_array['Europe/Kaliningrad'] = '+03:00';
    $zone_array['Europe/Kiev'] = '+02:00';
    $zone_array['Europe/Lisbon'] = '+00:00';
    $zone_array['Europe/Ljubljana'] = '+01:00';
    $zone_array['Europe/London'] = '+00:00';
    $zone_array['Europe/Luxembourg'] = '+01:00';
    $zone_array['Europe/Madrid'] = '+01:00';
    $zone_array['Europe/Malta'] = '+01:00';
    $zone_array['Europe/Mariehamn'] = '+02:00';
    $zone_array['Europe/Minsk'] = '+03:00';
    $zone_array['Europe/Monaco'] = '+01:00';
    $zone_array['Europe/Moscow'] = '+04:00';
    $zone_array['Europe/Nicosia'] = '+02:00';
    $zone_array['Europe/Oslo'] = '+01:00';
    $zone_array['Europe/Paris'] = '+01:00';
    $zone_array['Europe/Podgorica'] = '+01:00';
    $zone_array['Europe/Prague'] = '+01:00';
    $zone_array['Europe/Riga'] = '+02:00';
    $zone_array['Europe/Rome'] = '+01:00';
    $zone_array['Europe/Samara'] = '+04:00';
    $zone_array['Europe/San_Marino'] = '+01:00';
    $zone_array['Europe/Sarajevo'] = '+01:00';
    $zone_array['Europe/Simferopol'] = '+02:00';
    $zone_array['Europe/Skopje'] = '+01:00';
    $zone_array['Europe/Sofia'] = '+02:00';
    $zone_array['Europe/Stockholm'] = '+01:00';
    $zone_array['Europe/Tallinn'] = '+02:00';
    $zone_array['Europe/Tirane'] = '+01:00';
    $zone_array['Europe/Tiraspol'] = '+02:00';
    $zone_array['Europe/Uzhgorod'] = '+02:00';
    $zone_array['Europe/Vaduz'] = '+01:00';
    $zone_array['Europe/Vatican'] = '+01:00';
    $zone_array['Europe/Vienna'] = '+01:00';
    $zone_array['Europe/Vilnius'] = '+02:00';
    $zone_array['Europe/Volgograd'] = '+04:00';
    $zone_array['Europe/Warsaw'] = '+01:00';
    $zone_array['Europe/Zagreb'] = '+01:00';
    $zone_array['Europe/Zaporozhye'] = '+02:00';
    $zone_array['Europe/Zurich'] = '+01:00';
    $zone_array['GB'] = '+00:00';
    $zone_array['GB-Eire'] = '+00:00';
    $zone_array['GMT'] = '+00:00';
    $zone_array['GMT+0'] = '+00:00';
    $zone_array['GMT0'] = '+00:00';
    $zone_array['GMT-0'] = '+00:00';
    $zone_array['Greenwich'] = '+00:00';
    $zone_array['Hong Kong'] = '+08:00';
    $zone_array['HST'] = '-10:00';
    $zone_array['Iceland'] = '+00:00';
    $zone_array['Indian/Antananarivo'] = '+03:00';
    $zone_array['Indian/Chagos'] = '+06:00';
    $zone_array['Indian/Christmas'] = '+07:00';
    $zone_array['Indian/Cocos'] = '+06:30';
    $zone_array['Indian/Comoro'] = '+03:00';
    $zone_array['Indian/Kerguelen'] = '+05:00';
    $zone_array['Indian/Mahe'] = '+04:00';
    $zone_array['Indian/Maldives'] = '+05:00';
    $zone_array['Indian/Mauritius'] = '+04:00';
    $zone_array['Indian/Mayotte'] = '+03:00';
    $zone_array['Indian/Reunion'] = '+04:00';
    $zone_array['Iran'] = '+03:30';
    $zone_array['Israel'] = '+02:00';
    $zone_array['Jamaica'] = '-05:00';
    $zone_array['Japan'] = '+09:00';
    $zone_array['JST-9'] = '+09:00';
    $zone_array['Kwajalein'] = '+12:00';
    $zone_array['Libya'] = '+02:00';
    $zone_array['MET'] = '+01:00';
    $zone_array['Mexico/BajaNorte'] = '-08:00';
    $zone_array['Mexico/BajaSur'] = '-07:00';
    $zone_array['Mexico/General'] = '-06:00';
    $zone_array['MST'] = '-07:00';
    $zone_array['MST7MDT'] = '-07:00';
    $zone_array['Navajo'] = '-07:00';
    $zone_array['NZ'] = '+12:00';
    $zone_array['NZ-CHAT'] = '+12:45';
    $zone_array['Pacific/Apia'] = '+13:00';
    $zone_array['Pacific/Auckland'] = '+12:00';
    $zone_array['Pacific/Chatham'] = '+12:45';
    $zone_array['Pacific/Chuuk'] = '+10:00';
    $zone_array['Pacific/Easter'] = '-06:00';
    $zone_array['Pacific/Efate'] = '+11:00';
    $zone_array['Pacific/Enderbury'] = '+13:00';
    $zone_array['Pacific/Fakaofo'] = '+13:00';
    $zone_array['Pacific/Fiji'] = '+12:00';
    $zone_array['Pacific/Funafuti'] = '+12:00';
    $zone_array['Pacific/Galapagos'] = '-06:00';
    $zone_array['Pacific/Gambier'] = '-09:00';
    $zone_array['Pacific/Guadalcanal'] = '+11:00';
    $zone_array['Pacific/Guam'] = '+10:00';
    $zone_array['Pacific/Honolulu'] = '-10:00';
    $zone_array['Pacific/Johnston'] = '-10:00';
    $zone_array['Pacific/Kiritimati'] = '+14:00';
    $zone_array['Pacific/Kosrae'] = '+11:00';
    $zone_array['Pacific/Kwajalein'] = '+12:00';
    $zone_array['Pacific/Majuro'] = '+12:00';
    $zone_array['Pacific/Marquesas'] = '-09:30';
    $zone_array['Pacific/Midway'] = '-11:00';
    $zone_array['Pacific/Nauru'] = '+12:00';
    $zone_array['Pacific/Niue'] = '-11:00';
    $zone_array['Pacific/Norfolk'] = '+11:30';
    $zone_array['Pacific/Noumea'] = '+11:00';
    $zone_array['Pacific/Pago_Pago'] = '-11:00';
    $zone_array['Pacific/Palau'] = '+09:00';
    $zone_array['Pacific/Pitcairn'] = '-08:00';
    $zone_array['Pacific/Pohnpei'] = '+11:00';
    $zone_array['Pacific/Ponape'] = '+11:00';
    $zone_array['Pacific/Port_Moresby'] = '+10:00';
    $zone_array['Pacific/Rarotonga'] = '-10:00';
    $zone_array['Pacific/Saipan'] = '+10:00';
    $zone_array['Pacific/Samoa'] = '-11:00';
    $zone_array['Pacific/Tahiti'] = '-10:00';
    $zone_array['Pacific/Tarawa'] = '+12:00';
    $zone_array['Pacific/Tongatapu'] = '+13:00';
    $zone_array['Pacific/Truk'] = '+10:00';
    $zone_array['Pacific/Wake'] = '+12:00';
    $zone_array['Pacific/Wallis'] = '+12:00';
    $zone_array['Pacific/Yap'] = '+10:00';
    $zone_array['Poland'] = '+01:00';
    $zone_array['Portugal'] = '+00:00';
    $zone_array['PRC'] = '+08:00';
    $zone_array['PST8PDT'] = '-08:00';
    $zone_array['ROC'] = '+08:00';
    $zone_array['ROK'] = '+09:00';
    $zone_array['Singapore'] = '+08:00';
    $zone_array['Turkey'] = '+02:00';
    $zone_array['UCT'] = '+00:00';
    $zone_array['Universal'] = '+00:00';
    $zone_array['US/Alaska'] = '-09:00';
    $zone_array['US/Aleutian'] = '-10:00';
    $zone_array['US/Arizona'] = '-07:00';
    $zone_array['US/Central'] = '-06:00';
    $zone_array['US/Eastern'] = '-05:00';
    $zone_array['US/East-Indiana'] = '-05:00';
    $zone_array['US/Hawaii'] = '-10:00';
    $zone_array['US/Indiana-Starke'] = '-06:00';
    $zone_array['US/Michigan'] = '-05:00';
    $zone_array['US/Mountain'] = '-07:00';
    $zone_array['US/Pacific'] = '-08:00';
    $zone_array['US/Pacific-New'] = '-08:00';
    $zone_array['US/Samoa'] = '-11:00';
    $zone_array['UTC'] = '+00:00';
    $zone_array['WET'] = '+00:00';
    $zone_array['W-SU'] = '+04:00';
    $zone_array['Zulu'] = '+00:00';
    $error = 0;
    $values = $form_state->getValues();
    db_query("truncate table timezone_clock_blocks");
    for ($i = 0; $i < 3; $i++) {
      $timezone = $values['names_fieldset']['form_inner_fieldset'][$i]['timezone'];
      $timeformat = $values['names_fieldset']['form_inner_fieldset'][$i]['timeformat'];
      $country = $values['names_fieldset']['form_inner_fieldset'][$i]['country_name'];
      $selected_offset = str_replace(":", ".", $zone_array[$timezone]);
      if ($timezone != '') {
        try {
          db_insert('timezone_clock_blocks')
            ->fields(array(
              'country' => $country,
              'timezone' => $timezone,
              'offset' => $selected_offset,
              'time_format' => $timeformat,
            ))
            ->execute();
        }
        catch (Exception $e) {
          drupal_set_message($e);
          $error = 1;
        }
      } // $timezone
    } // end loop
    if ($error == 0) {
      drupal_set_message($this->t("Timezone clock configuration saved."));
    }
  }

} // class
